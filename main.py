import sys

from PyQt5.QtWidgets import QApplication

from view.main_window import MainWindow


def run():
    application = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    return application.exec()


if __name__ == '__main__':
    exit(run())
