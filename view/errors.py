from PyQt5.QtWidgets import QMessageBox


def alert(window, message):
    QMessageBox.question(window, 'Error', str(message), QMessageBox.Ok)
