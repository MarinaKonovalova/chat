from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QVBoxLayout, QGroupBox, QFormLayout, QLabel, QLineEdit


class AddContactWindow(QDialog):
    def __init__(self):
        super(AddContactWindow, self).__init__()
        self.create_form_group_box()

        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        button_box.accepted.connect(self.accept_)
        button_box.rejected.connect(self.cancel_)

        main_layout = QVBoxLayout()
        main_layout.addWidget(self.form_group_box)
        main_layout.addWidget(button_box)
        self.setLayout(main_layout)

        self.setWindowTitle("Add contact")

        self.setFixedSize(250, 200)

        self.xButton = False
        self.myAccepted = False

    def cancel_(self):
        self.close()

    def accept_(self):
        self.myAccepted = True
        self.close()

    def create_form_group_box(self):
        self.form_group_box = QGroupBox()
        layout = QFormLayout()
        name = QLineEdit()
        address = QLineEdit()
        port = QLineEdit()

        layout.addRow(QLabel("Name:"), name)
        layout.addRow(QLabel("Address:"), address)
        layout.addRow(QLabel("Port:"), port)

        self.__contact_name = name
        self.__contact_address = address
        self.__contact_port = port

        self.form_group_box.setLayout(layout)

    @property
    def name(self):
        return self.__contact_name

    @property
    def address(self):
        return self.__contact_address

    @property
    def port(self):
        return self.__contact_port
