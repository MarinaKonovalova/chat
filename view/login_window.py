import logging

from PyQt5 import QtGui
from PyQt5.QtCore import QRegExp
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QFormLayout, QGroupBox, QLabel, QLineEdit, QVBoxLayout

from model.chat import Chat
from view.errors import alert

logger = logging.getLogger(__name__)


def is_valid(form_nput):
    validator = form_nput.validator()
    return form_nput.validator().validate(form_nput.text(), 0)[0] == validator.Acceptable


class LoginWindow(QDialog):

    def set_ok_enabled(self, *a, **kw):
        is_enabled = True
        is_enabled &= is_valid(self.port_field)
        is_enabled &= is_valid(self.username_field)
        self.ok_btn.setEnabled(is_enabled)

    def __init__(self):
        super(LoginWindow, self).__init__()
        self.username_field = QLineEdit()
        self.port_field = QLineEdit()
        self.username_field.setValidator(QtGui.QRegExpValidator(QRegExp("[a-z-A-Z_0-9]+")))
        self.port_field.setValidator(QtGui.QIntValidator(1024, 65535))

        self.form_group_box = self.create_form_group_box(self.username_field, self.port_field)
        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept_)
        button_box.rejected.connect(self.cancel_)
        self.ok_btn = button_box.button(QDialogButtonBox.Ok)
        self.ok_btn.setEnabled(False)
        self.username_field.textChanged.connect(self.set_ok_enabled)
        self.port_field.textChanged.connect(self.set_ok_enabled)

        main_layout = QVBoxLayout()
        main_layout.addWidget(self.form_group_box)
        main_layout.addWidget(button_box)
        self.setLayout(main_layout)

        self.setWindowTitle("Login")

        self.setFixedSize(250, 150)

        self.xButton = False
        self.isAccepted = False
        self.isClosed = False

    def closeEvent(self, event):
        if not self.isAccepted:
            self.isClosed = True

    def cancel_(self):
        self.isClosed = True
        self.close()

    def accept_(self):
        self.isAccepted = True
        self.close()

    def create_form_group_box(self, name, port):
        form_group_box = QGroupBox()
        layout = QFormLayout()
        layout.addRow(QLabel("Name:"), name)
        layout.addRow(QLabel("Port:"), port)
        form_group_box.setLayout(layout)
        return form_group_box

    @property
    def username(self):
        return self.username_field.text()

    @property
    def address(self):
        # return '0.0.0.0'
        return 'localhost'

    @property
    def port(self):
        return int(self.port_field.text())

    def init_chat(self):
        try:
            return Chat(self.address, self.port, self.username)
        except Exception as e:
            logger.exception(e)
            alert(self, e)
