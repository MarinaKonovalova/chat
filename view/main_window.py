import logging

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QHBoxLayout, QLabel, QListWidget, QPushButton, QTextEdit, QVBoxLayout, QWidget

from model.chat import Chat
from model.contact import Contact
from model.errors import ContactAlreadyExists
from model.list_dialogs_item import ListContactsItem
from model.message import Message, MessageKind
from view.add_contact_window import AddContactWindow
from view.errors import alert
from view.login_window import LoginWindow

logger = logging.getLogger(__name__)


class MainWindowUI:
    def __init__(self):
        self.dialog_view = QTextEdit()
        self.dialog_view.setReadOnly(True)
        self.message_input = QTextEdit()

        self.contact_list_area = QListWidget()

        self.main_layout = QHBoxLayout()
        self.main_layout.addWidget(self.contact_list_area, 0, Qt.AlignLeft)

        self.right_layout = QVBoxLayout()
        self.current_dialog_label = QLabel("")
        self.right_layout.addWidget(self.current_dialog_label, 0, Qt.AlignLeft)
        self.right_layout.addWidget(self.dialog_view, 0, Qt.AlignLeft)

        self.controls_layout = QHBoxLayout()
        self.controls_layout.addWidget(self.message_input, 0, Qt.AlignLeft)

        self.buttons_layout = QVBoxLayout()

        self.send_button = QPushButton("Send")
        self.add_contact_button = QPushButton("Add contact")

        self.buttons_layout.addWidget(self.send_button, 0, Qt.AlignLeft)
        self.buttons_layout.addWidget(self.add_contact_button, 0, Qt.AlignLeft)

        self.controls_layout.addLayout(self.buttons_layout, 0)

        self.right_layout.addLayout(self.controls_layout, 0)

        self.main_layout.addLayout(self.right_layout)


class MainWindow(QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__chat = self.login()

        self.ui = MainWindowUI()
        self.ui.send_button.clicked.connect(self.send_button_clicked)
        self.ui.add_contact_button.clicked.connect(self.add_contact_button_clicked)

        self.__chat.on_message_received += self.on_msg_received

        self.current_dialog = self.__chat.my_contact
        contact_item = ListContactsItem(self.__chat.my_contact)
        self.ui.contact_list_area.addItem(contact_item)
        self.ui.contact_list_area.currentItemChanged.connect(self.change_current_dialog)

        self.setWindowTitle('Chat - ' + self.__chat.name)
        self.setFixedSize(650, 500)
        self.setLayout(self.ui.main_layout)

    def login(self) -> Chat:
        chat = None
        while not chat:
            login_window = LoginWindow()
            login_window.exec_()
            if login_window.isClosed:
                exit()
            chat = login_window.init_chat()
        return chat

    def change_current_dialog(self, new, old):
        if new:
            self.current_dialog = new.contact

    @property
    def current_dialog(self):
        return self._current_dialog

    @current_dialog.setter
    def current_dialog(self, contact: Contact):

        self._current_dialog = contact
        self.ui.current_dialog_label.setText(contact.name)
        self.ui.dialog_view.clear()
        self.ui.dialog_view.setText(self.__chat.get_formatted_messages(contact))

    def send_button_clicked(self):
        message = Message(
            sender=self.__chat.my_contact,
            receiver=self.current_dialog,
            data=self.ui.message_input.toPlainText()
        )
        try:
            self.__chat.send(message)
        except ConnectionError as e:
            return alert(self, e)
        self.add_text_message(message.receiver, message)
        self.ui.message_input.clear()

    def add_contact_button_clicked(self):
        self.add_contact()
        self.share_contacts()

    def share_contacts(self):
        for contact in list(self.__chat.contacts):
            try:
                self.__chat.send(Message(
                    sender=self.__chat.my_contact,
                    receiver=contact,
                    data=list(self.__chat.contacts.keys()),
                    kind=MessageKind.contact
                ))
            except ConnectionError as e:
                logger.info(e)

    def update_contacts(self, message: Message):
        self.__chat.add_contact(*message.data)
        self.ui.contact_list_area.clear()
        for c in self.__chat.contacts:
            contact_item = ListContactsItem(c)
            self.ui.contact_list_area.addItem(contact_item)

    def add_text_message(self, contact, message: Message):
        self.__chat.add_message(contact, message)
        if self.current_dialog == contact:
            self.ui.dialog_view.append(str(message))

    def on_msg_received(self, message: dict):
        message = Message.from_json(message)
        if message.kind == MessageKind.contact:
            return self.update_contacts(message)
        elif message.kind == MessageKind.text:
            return self.add_text_message(message.sender, message)
        logger.warning('Unknown message kind received', message.to_json())

    def add_contact(self):
        add_contact_window = AddContactWindow()
        add_contact_window.exec_()

        if add_contact_window.myAccepted:
            try:
                name = add_contact_window.name.text()
                address = add_contact_window.address.text()
                port = int(add_contact_window.port.text())
            except ValueError as e:
                return alert(self, e)
            else:
                contact = Contact(name, address, port)
                try:
                    self.__chat.add_contact(contact, raise_exists=True)
                except ContactAlreadyExists as e:
                    return alert(self, e)
                contact_item = ListContactsItem(contact)
                self.ui.contact_list_area.addItem(contact_item)
