class MessageReceivedEvent:
    def __init__(self):
        self.__handlers = []

    def __call__(self, message):
        for handler in self.__handlers:
            handler(message)

    def __iadd__(self, other):
        self.__handlers.append(other)

    def __isub__(self, other):
        self.__handlers.remove(other)
