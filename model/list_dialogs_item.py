from PyQt5.QtWidgets import QListWidgetItem


class ListContactsItem(QListWidgetItem):
    def __init__(self, contact):
        super(ListContactsItem, self).__init__()
        self.contact = contact
        self.setText(contact.name)
