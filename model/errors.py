class BaseError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class ContactAlreadyExists(BaseError):
    def __init__(self, msg='Contact already exists'):
        super().__init__(msg)


class ContactNotFound(BaseError):
    def __init__(self, msg='Contact not found'):
        super().__init__(msg)
