class Contact:
    def __init__(self, name, address, port):
        self.name = name
        self.address = address
        self.port = port
        self.messages = []

    @property
    def id(self):
        return hash((self.address, self.port))

    def __eq__(self, other):
        return self.id == other.id

    def add_message(self, message):
        self.messages.append(message)

    def __hash__(self):
        return self.id

    def to_json(self):
        return {
            'name': self.name,
            'address': self.address,
            'port': self.port,
        }

    @classmethod
    def from_json(cls, data):
        return Contact(**data)
