import base64
import enum
from typing import Any

from model.contact import Contact


class MessageKind(enum.IntEnum):
    text = 1
    contact = 2


def to_base64(data: str):
    return base64.encodebytes(data.encode('utf-8')).decode('ascii')


def from_base64(data: str):
    return base64.decodebytes(data.encode('ascii')).decode('utf-8')


class Message:

    def __init__(self, sender: Contact, receiver: Contact, data: Any, kind=MessageKind.text):
        self.sender = sender
        self.receiver = receiver
        self.data = data
        self.kind = kind

    def __str__(self):
        return '{}: {}'.format(self.sender.name, self.data)

    def to_json(self):
        res = {
            'kind': self.kind,
            'sender': self.sender.to_json(),
            'receiver': self.receiver.to_json(),
        }
        if self.kind == MessageKind.text:
            res['data'] = to_base64(self.data)
        elif self.kind == MessageKind.contact:
            res['data'] = [c.to_json() for c in self.data]
        return res

    @classmethod
    def from_json(cls, msg):
        msg['kind'] = MessageKind(msg['kind'])
        msg['sender'] = Contact.from_json(msg['sender'])
        msg['receiver'] = Contact.from_json(msg['receiver'])
        if msg['kind'] == MessageKind.contact:
            msg['data'] = [Contact.from_json(c) for c in msg['data']]
        if msg['kind'] == MessageKind.text:
            msg['data'] = from_base64(msg['data'])
        return Message(**msg)
