from model.client import Client
from model.contact import Contact
from model.errors import ContactAlreadyExists, ContactNotFound
from model.message import Message
from model.message_received import MessageReceivedEvent
from model.server import Server


class Chat:
    def __init__(self, address, port, name):
        self.on_message_received = MessageReceivedEvent()
        self.contacts = {}
        self.name = name
        self.my_contact = Contact(name, address, port)
        self.add_contact(self.my_contact, raise_exists=True)
        self.__client = Client()
        self.__server = Server(address, port, 10, self.on_message_received)
        self.__server.start()

    def add_message(self, contact: Contact, message: Message):
        self.contacts[contact].add_message(message)

    def get_formatted_messages(self, contact: Contact):
        return '\n'.join([str(m) for m in self.contacts[contact].messages])

    def send(self, message):
        if message.receiver not in self.contacts:
            raise ContactNotFound

        contact = self.contacts[message.receiver]
        self.__client.send(contact, message)

    def add_contact(self, *contacts: Contact, raise_exists=False):
        if raise_exists:
            for c in contacts:
                if c in self.contacts:
                    raise ContactAlreadyExists
        for c in contacts:
            if c not in self.contacts:
                self.contacts[c] = c
