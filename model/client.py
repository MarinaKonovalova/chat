import json
import socket

from model.contact import Contact
from model.message import Message


class Client:
    def send(self, contact: Contact, message: Message):
        data = json.dumps(message.to_json())
        sock = socket.socket()
        try:
            sock.connect((contact.address, contact.port))
            sock.send(data.encode())
        except Exception as e:
            raise ConnectionError("Can't connect to {}:{}. reason: {}".format(contact.address, contact.port, e))
        finally:
            sock.close()
