import json
import socket
import threading
from functools import partial


class Server:

    def __init__(self, address, port, clients_listen_count, message_received):
        super().__init__()
        self.__sock = socket.socket()
        self.__sock.bind((address, port))
        self.__sock.listen(clients_listen_count)
        self.__message_received = message_received
        self.__thr = threading.Thread(target=self.run, args=(), kwargs={})

    def start(self):
        self.__thr.daemon = True
        self.__thr.start()

    def run(self):

        while True:
            conn, addr = self.__sock.accept()

            while True:
                data = b''.join(iter(partial(conn.recv, 1024), b''))
                if not data:
                    break
                message = json.loads(data.decode())
                self.__message_received(message)
            conn.close()
